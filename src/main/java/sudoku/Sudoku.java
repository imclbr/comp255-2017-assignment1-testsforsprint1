
package org.bitbucket.sudoku;

/**
 * Sudoku
 */
public class Sudoku {

  /**
   * Constructor for Sudoku
   *
   * @param g   The grid that defines the sudoku
   */
  public Sudoku(int[][] g) {
    theGrid = g;
  }

  /**
   * Secondary constructor for Sudoku
   *
   * @param g   The grid that defines the sudoku
   * @param e   The value that denotes an empty cell
   */
  public Sudoku(int[][] g, int e) {
    // FIXME
  }
  /**
   * The n x m grid that defines the Sudoku
   */
  private int[][] theGrid;

  /**
   *  Check validity of a Sudoku grid
   *
   *  @return true if and only if theGrid is a valid Sudoku
   */
  public boolean isValid()  {
    //  check size of grid
    if (theGrid.length == 0)
      return true;
    else {
      //  Grid is not empty
      // FIXME
      return false;
    }
  }

  /**
   *   Attempt to compute a solution to the Sudoku
   *
   *   @return  A grid with possibly less empty cells than in theGrid (but not more)
   *
   *   @note    If there is no empty cell in the result, then the Sudoku is solved,
   *            otherwise it is not
   */
  public int[][] solve() {
    // FIXME: write the code for this in Sprint 2
    return theGrid;
  }

  /**
   *   Attempt to efficiently compute a solution to the Sudoku
   *
   *   @return  A grid with possibly less empty cells than in theGrid (but not more)
   *
   *   @note    If there is no empty cell in the result, then the Sudoku is solved,
   *            otherwise it is not
   */
  public int[][] fastSolve() {
    // FIXME: write the code for this in Sprint 3
    return theGrid;
  }
}
